﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IS2GRUPO9.Models;
using IS2GRUPO9.Models.BD;

namespace IS2GRUPO9.Repositorios
{
    public class RequerimientoRepositorio : IRequerimientoRepostorio
    {
        public bool Actualizar(RequerimientoProyecto requerimiento)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    ctx.RequerimientosProyectos.Update(requerimiento);
                    ctx.SaveChanges();
                }
            } catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public bool Crear(RequerimientoProyecto requerimiento)
        {
            try
            {
                requerimiento.Id = 0;
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    ctx.RequerimientosProyectos.Add(requerimiento);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public bool Eliminar(RequerimientoProyecto requerimiento)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    ctx.RequerimientosProyectos.Remove(requerimiento);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public RequerimientoProyecto Obtener(int id)
        {
            RequerimientoProyecto requerimiento = null;

            using (IngeSoftContext ctx = new IngeSoftContext())
            {
                requerimiento = ctx.RequerimientosProyectos.FirstOrDefault(x => x.Id == id);
            }

            return requerimiento;
        }
    }
}
