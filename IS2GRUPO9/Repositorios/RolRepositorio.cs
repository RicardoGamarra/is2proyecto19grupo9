﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IS2GRUPO9.Models;
using IS2GRUPO9.Models.BD;

namespace IS2GRUPO9.Repositorios
{
    public class RolRepositorio : IRolRepositorio
    {
        static List<Rol> fakeRols = new List<Rol>()
        {
            new Rol() { Id = 1, Descripcion = "Concede accesos para interactuar con proyectos.", Nombre = "DESARROLLADOR"},
            new Rol() { Id = 2, Descripcion = "Concede accesos para administrar proyectos y usuarios.", Nombre = "Administrador"},
            new Rol() { Id = 3, Descripcion = "Concede accesos para administrar proyectos y usuarios.", Nombre = "Administrador"}
        };
        public bool Actualizar(Rol rol)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    //proyecto.FechaCreacion = DateTime.Now;
                    ctx.Roles.Update(rol);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            };
        }

        public bool Crear(Rol rol)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    //proyecto.FechaCreacion = DateTime.Now;
                    ctx.Roles.Add(rol);
                    ctx.SaveChanges();
                }

                return true;
            } catch (Exception ex)
            {
                return false;
            };
        }

        public bool Eliminar(int idRol)
        {
            Rol rol = this.Obtener(idRol);
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    //proyecto.FechaCreacion = DateTime.Now;
                    ctx.Roles.Remove(rol);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            };
        }

        public Rol Obtener(int idRol)
        {
            Rol rol = null;
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    //proyecto.FechaCreacion = DateTime.Now;
                    rol = ctx.Roles.FirstOrDefault(r => r.Id == idRol);
                }
            }
            catch (Exception ex)
            {
                //// TODO: AÑADIR LOGS.
            };
            return rol;
        }

        public List<Rol> ObtenerTodos()
        {
            List<Rol> roles = new List<Rol>();
            using (IngeSoftContext ctx = new IngeSoftContext())
            {
                //proyecto.FechaCreacion = DateTime.Now;
                roles = ctx.Roles.ToList();
            }
            return roles;
        }
    }
}
