﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IS2GRUPO9.Models;
using IS2GRUPO9.Models.BD;

namespace IS2GRUPO9.Repositorios
{
    public class LineaBaseRepositorio : ILineaBaseRepositorio
    {
        private List<LineaBase> fakeLineasBase = new List<LineaBase>();

        public LineaBaseRepositorio()
        {
        }

        public LineaBase ObtenerLineaBase(int idLineaBase)
        {
            LineaBase lineaBase = null;
            using(IngeSoftContext ctx = new IngeSoftContext())
            {
                lineaBase = ctx.LineasBase.FirstOrDefault(x => x.Codigo == idLineaBase);
            }
            return lineaBase;
        }

        public List<LineaBase> ObtenerTodas()
        {
            List<LineaBase> lineasBase = new List<LineaBase>();
            using (IngeSoftContext ctx = new IngeSoftContext())
            {
                lineasBase = ctx.LineasBase.ToList();
            }
            return lineasBase;
        }

        public List<LineaBase> ObtenerLineasBaseActivas()
        {
            List<LineaBase> lineasBaseActivas = new List<LineaBase>();
            using (IngeSoftContext ctx = new IngeSoftContext())
            {
                lineasBaseActivas = ctx.LineasBase.Where(x => x.Estado == LineaBase.ESTADO_ACTIVA).ToList();
            }
            return lineasBaseActivas;
        }

        public bool CrearLineaBase(LineaBase lineaBase)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    ctx.LineasBase.Add(lineaBase);
                    ctx.SaveChanges();
                }
            } catch (Exception ex)
            {
                return false;
            }
            
            return true;
        }

        public bool ActualizarLineaBase(LineaBase lineaBase)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    ctx.LineasBase.Update(lineaBase);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public bool EliminarLineaBase(int idLineaBase)
        {
            LineaBase lineaBase = this.ObtenerLineaBase(idLineaBase);
            if (lineaBase == null)
            {
                return false;
            }

            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    ctx.LineasBase.Update(lineaBase);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
    }
}
