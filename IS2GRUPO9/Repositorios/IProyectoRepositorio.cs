﻿using IS2GRUPO9.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Repositorios
{
    public interface IProyectoRepositorio
    {
        List<Proyecto> ObtenerTodos();

        Proyecto Obtener(int idProyecto);

        bool Crear(Proyecto proyecto);

        bool Actualizar(Proyecto proyecto);

        bool Delete(int idProyecto);

        List<Proyecto> ObtenerProyectosUsuario(int idUsuario);

        List<RequerimientoProyecto> ObtenerRequerimientos(int proyecto);

        bool AgregarRequerimiento(RequerimientoProyecto requerimiento);
    }
}
