﻿using IS2GRUPO9.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Repositorios
{
    public interface ILineaBaseItemRepositorio
    {
        List<LineaBaseItem> ObtenerPorLineaBase(int idLineaBase);

        bool AgregarItems(int idLineaBase, string[] items);

        bool ActualizarItemsPorLineaBase(int idLineaBase, string[] items); 
    }
}
