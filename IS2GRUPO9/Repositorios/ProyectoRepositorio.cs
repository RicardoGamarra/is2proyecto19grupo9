﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using IS2GRUPO9.Models;
using IS2GRUPO9.Models.BD;

namespace IS2GRUPO9.Repositorios
{
    public class ProyectoRepositorio : IProyectoRepositorio
    {
        public bool Actualizar(Proyecto proyecto)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    //proyecto.FechaCreacion = DateTime.Now;
                    ctx.Proyectos.Update(proyecto);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public bool Crear(Proyecto proyecto)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    proyecto.FechaCreacion = DateTime.Now;
                    ctx.Proyectos.Add(proyecto);
                    ctx.SaveChanges();
                }
            } catch (Exception ex)
            {
                return false;
            }
            
            return true;
        }

        public bool Delete(int idProyecto)
        {
            Proyecto proyecto = this.Obtener(idProyecto);
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    //proyecto.FechaCreacion = DateTime.Now;
                    ctx.Proyectos.Remove(proyecto);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        public Proyecto Obtener(int idProyecto)
        {
            Proyecto proyecto = null;
            using (IngeSoftContext ctx = new IngeSoftContext())
            {
                proyecto = ctx.Proyectos.FirstOrDefault(x => x.Id == idProyecto);
            }
            return proyecto;
        }

        public List<Proyecto> ObtenerTodos()
        {
            List<Proyecto> proyectos = new List<Proyecto>();
            using (IngeSoftContext ctx = new IngeSoftContext())
            {
                proyectos = ctx.Proyectos.ToList();
            }
            return proyectos;
        }

        public List<Proyecto> ObtenerProyectosUsuario(int idUsuario)
        {
            List<Proyecto> proyectos = new List<Proyecto>();
            using (IngeSoftContext ctx = new IngeSoftContext())
            {
                var proyectosUsuario = ctx.Proyectos.Where(x => x.IdEncargado == idUsuario).ToList();
                if (proyectosUsuario != null)
                {
                    proyectos = proyectosUsuario;
                }
            }
            return proyectos;
        }

        public List<RequerimientoProyecto> ObtenerRequerimientos(int proyecto)
        {
            List<RequerimientoProyecto> requerimientos = new List<RequerimientoProyecto>();
            using (IngeSoftContext ctx = new IngeSoftContext())
            {
                var requerimientosProyecto = ctx.RequerimientosProyectos.Where(x => x.IdProyecto == proyecto).ToList();
                if (requerimientosProyecto != null)
                {
                    requerimientos = requerimientosProyecto;
                }
            }
            return requerimientos;
        }

        public bool AgregarRequerimiento(RequerimientoProyecto requerimiento)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    ctx.RequerimientosProyectos.Add(requerimiento);
                    ctx.SaveChanges();
                }
            } catch (Exception ex)
            {
                return false;
            }
            
            return true;
        }
    }
}
