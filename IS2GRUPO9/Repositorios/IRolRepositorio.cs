﻿using IS2GRUPO9.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Repositorios
{
    public interface IRolRepositorio
    {
        List<Rol> ObtenerTodos();

        Rol Obtener(int idRol);

        bool Actualizar(Rol rol);

        bool Crear(Rol rol);

        bool Eliminar(int idRol);
    }
}
