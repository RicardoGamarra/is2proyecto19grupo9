﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IS2GRUPO9.Models;
using IS2GRUPO9.Models.BD;

namespace IS2GRUPO9.Repositorios
{
    public class UsuarioRepositorio : IUsuarioRepositorio
    {
        public bool ActualizarUsuario(Usuario usuario)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    //proyecto.FechaCreacion = DateTime.Now;
                    ctx.Usuarios.Update(usuario);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AgregarUsuario(Usuario usuario)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    //proyecto.FechaCreacion = DateTime.Now;
                    ctx.Usuarios.Add(usuario);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool EliminarUsuario(int idUsuario)
        {
            Usuario usuario = this.ObtenerUsuario(idUsuario);
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    //proyecto.FechaCreacion = DateTime.Now;
                    ctx.Usuarios.Remove(usuario);
                    ctx.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Usuario ObtenerUsuario(string nombreUsuario, string password)
        {
            Usuario usuario = null;
            using (IngeSoftContext ctx = new IngeSoftContext())
            {
                usuario = ctx.Usuarios.FirstOrDefault(x => x.NombreUsuario == nombreUsuario && x.Password == password);
            }
            return usuario;
        }

        public List<Usuario> ObtenerTodos()
        {
            var usuarios = new List<Usuario>();

            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    //proyecto.FechaCreacion = DateTime.Now;
                    usuarios = ctx.Usuarios.ToList();
                }
            }
            catch (Exception ex)
            {
                ////TODO: AGREGAR LOGS.
            }

            return usuarios;
        }

        public Usuario ObtenerUsuario(int idUsuario)
        {
            Usuario usuario = null;
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    //proyecto.FechaCreacion = DateTime.Now;
                    usuario = ctx.Usuarios.FirstOrDefault(x => x.Id == idUsuario);
                }
            }
            catch (Exception ex)
            {
                ////TODO: AGREGAR LOGS.
            }
            return usuario;
        }



        private List<Usuario> FakeUsers()
        {
            List<Usuario> fakeUsers = new List<Usuario>();

            fakeUsers.Add(new Usuario()
            {
                Apellido = "Test",
                IdRol = 1,
                Id = 1,
                Nombre = "Ángel",
                NombreUsuario = "ANGELB",
                Password = "1234556"
            });

            fakeUsers.Add(new Usuario()
            {
                Apellido = "Gonzalez",
                IdRol = 2,
                Id = 2,
                Nombre = "Juan",
                NombreUsuario = "JUANG",
                Password = "12342"
            });

            return fakeUsers;
        }
    }
}
