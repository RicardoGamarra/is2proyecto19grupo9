﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IS2GRUPO9.Models;
using IS2GRUPO9.Models.BD;

namespace IS2GRUPO9.Repositorios
{
    public class LineaBaseItemRepositorio : ILineaBaseItemRepositorio
    {
        public List<LineaBaseItem> ObtenerPorLineaBase(int idLineaBase)
        {
            List<LineaBaseItem> items = new List<LineaBaseItem>();

            using (IngeSoftContext ctx = new IngeSoftContext())
            {
                items = ctx.LineaBaseItems.Where(x => x.IdLineaBase == idLineaBase).ToList();
            }

            return items;
        }
        
        public bool AgregarItems(int idLineaBase, string[] items)
        {
            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    foreach (var item in items)
                    {
                        ctx.LineaBaseItems.Add(new LineaBaseItem() { IdLineaBase = idLineaBase, Nombre = item });
                    }
                    ctx.SaveChanges();
                }
            } catch (Exception ex)
            {
                return false;
            }
            
            return true;
        }

        public bool ActualizarItemsPorLineaBase(int idLineaBase, string[] items)
        {
            List<LineaBaseItem> itemsActuales = this.ObtenerPorLineaBase(idLineaBase);

            try
            {
                using (IngeSoftContext ctx = new IngeSoftContext())
                {
                    foreach (var itemActual in itemsActuales)
                    {
                        var apariciones = items.Where(x => x == itemActual.Nombre).Count();
                        if (apariciones == 0)
                        {
                            ctx.LineaBaseItems.Remove(itemActual);
                        }
                    }
                    foreach (var itemNuevo in items)
                    {
                        var apariciones = itemsActuales.Where(x => x.Nombre == itemNuevo).Count();
                        if (apariciones == 0)
                        {
                            ctx.LineaBaseItems.Add(new LineaBaseItem() { Nombre = itemNuevo, IdLineaBase = idLineaBase });
                        }
                    }
                    ctx.SaveChanges();
                }
            } catch (Exception ex)
            {
                return false;
            }
            
            return true;
                
        }
    }
}
