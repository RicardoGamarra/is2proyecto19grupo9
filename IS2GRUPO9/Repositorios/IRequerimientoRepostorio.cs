﻿using IS2GRUPO9.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Repositorios
{
    public interface IRequerimientoRepostorio
    {
        bool Crear(RequerimientoProyecto requerimiento);

        bool Actualizar(RequerimientoProyecto requerimiento);

        RequerimientoProyecto Obtener(int id);

        bool Eliminar(RequerimientoProyecto requerimiento);
    }
}
