﻿using IS2GRUPO9.Models;
using IS2GRUPO9.Models.BD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Repositorios
{
    public interface IUsuarioRepositorio
    {
        List<Usuario> ObtenerTodos();

        Usuario ObtenerUsuario(int idUsuario);
        
        bool EliminarUsuario(int idUsuario);

        bool AgregarUsuario(Usuario usuario);

        bool ActualizarUsuario(Usuario usuario);

        Usuario ObtenerUsuario(string nombreUsuario, string password);
    }
}