﻿using IS2GRUPO9.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Repositorios
{
    public interface ILineaBaseRepositorio
    {
        LineaBase ObtenerLineaBase(int idLineaBase);

        List<LineaBase> ObtenerTodas();

        List<LineaBase> ObtenerLineasBaseActivas();

        bool CrearLineaBase(LineaBase lineaBase);

        bool ActualizarLineaBase(LineaBase lineaBase);

        bool EliminarLineaBase(int idLineaBase);
    }
}
