﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IS2GRUPO9.Models;
using IS2GRUPO9.Repositorios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IS2GRUPO9.Controllers
{
    public class UsuariosController : Controller
    {
        private readonly IUsuarioRepositorio usuariosRepositorio;
        private readonly IRolRepositorio rolRepositorio;

        public UsuariosController()
        {
            usuariosRepositorio = new UsuarioRepositorio();
            rolRepositorio = new RolRepositorio();
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            List<Usuario> usuarios = usuariosRepositorio.ObtenerTodos();
            //ViewBag.usuarios = usuarios;
            return View(usuarios);
        }

        public IActionResult Crear()
        {
            ViewBag.roles = this.RolesList();
            return View();
        }

        [HttpPost]
        public IActionResult Crear(Usuario usuario)
        {
            if (this.usuariosRepositorio.AgregarUsuario(usuario))
            {
                return this.RedirectToAction("Index");
            } else
            {
                ViewBag.errorMessage = "Ocurrió un error al crear el usuario.";
                return this.View(usuario);
            }
        }

        public IActionResult Editar(int idUsuario)
        {
            Usuario usuario = this.usuariosRepositorio.ObtenerUsuario(idUsuario);
            ViewBag.roles = this.RolesList(usuario.IdRol);
            return View(usuario);
        }

        [HttpPost]
        public IActionResult Editar(Usuario usuario)
        {
            if (!this.usuariosRepositorio.ActualizarUsuario(usuario))
            {
                ViewBag.errorMessage = "Ocurrió un error al actualizar el usuario.";
                ViewBag.roles = this.RolesList(usuario.IdRol);
                return this.View(usuario);
            }
            return this.RedirectToAction("Index");
        }

        public IActionResult Eliminar(int idUsuario)
        {
            Usuario usuario = this.usuariosRepositorio.ObtenerUsuario(idUsuario);
            return this.View(usuario);
        }

        [HttpPost]
        public IActionResult Eliminar(Usuario usuario)
        {
            if (!this.usuariosRepositorio.EliminarUsuario(usuario.Id))
            {
                ViewBag.errorMessage = "Ocurrió un error al actualizar el usuario.";
                ViewBag.roles = this.RolesList(usuario.IdRol);
                return this.View(usuario);
            }
            return this.RedirectToAction("Index");
        }

        private List<SelectListItem> RolesList(int? selectedValue = null)
        {
            List<Rol> roles = this.rolRepositorio.ObtenerTodos();
            List<SelectListItem> opcionesRol = new List<SelectListItem>();
            foreach(var rol in roles)
            {
                var optionItem = new SelectListItem(rol.Nombre, rol.Id.ToString());
                if (selectedValue != null && rol.Id == selectedValue)
                {
                    optionItem.Selected = true;
                }
                opcionesRol.Add(optionItem);
            }
            return opcionesRol;
        }
    }
}
