﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IS2GRUPO9.Models;
using IS2GRUPO9.Models.BD;
using IS2GRUPO9.Repositorios;

namespace IS2GRUPO9.Controllers
{
    public class RolController : Controller
    {
        private readonly IRolRepositorio rolRepositorio;

        public RolController()
        {
            this.rolRepositorio = new RolRepositorio();
        }

        public IActionResult Index()
        {
            List<Rol> roles = this.rolRepositorio.ObtenerTodos();
            return this.View(roles);
        }

        public IActionResult Crear()
        {
            return this.View();
        }

        [HttpPost]
        public IActionResult Crear(Rol rol)
        {
            this.rolRepositorio.Crear(rol);
            return this.RedirectToAction("Index");
        }

        public IActionResult Editar(int id)
        {
            Rol rol = this.rolRepositorio.Obtener(id);
            return this.View(rol);
        }

        [HttpPost]
        public IActionResult Editar(Rol rol)
        {
            this.rolRepositorio.Actualizar(rol);
            return this.RedirectToAction("Index");
        }

        public IActionResult Eliminar(int id)
        {
            Rol rol = this.rolRepositorio.Obtener(id);
            return this.View(rol);
        }

        [HttpPost]
        public IActionResult Eliminar(Rol rol)
        {
            this.rolRepositorio.Eliminar(rol.Id);
            return this.RedirectToAction("Index");
        }
    }
}