﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IS2GRUPO9.Models;
using IS2GRUPO9.Repositorios;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IS2GRUPO9.Controllers
{
    public class ProyectosController : Controller
    {
        private readonly IUsuarioRepositorio usuariosRepositorio;
        private readonly IProyectoRepositorio proyectoRepositorio;
        private readonly ILineaBaseRepositorio lineaBaseRepositorio;

        public ProyectosController()
        {
            usuariosRepositorio = new UsuarioRepositorio();
            proyectoRepositorio = new ProyectoRepositorio();
            lineaBaseRepositorio = new LineaBaseRepositorio();
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var model = this.proyectoRepositorio.ObtenerTodos();
            return View(model);
        }

        public IActionResult Details(int id)
        {
            Proyecto proyecto = this.proyectoRepositorio.Obtener(id);
            return View(proyecto);
        }

        public IActionResult Create()
        {
            ViewBag.users = this.GetUsuarios();
            ViewBag.lineasBase = this.GetLineasBase();
            return View();
        }

        [HttpPost]
        public IActionResult Create(Proyecto proyecto)
        {
            var creacionOk = this.proyectoRepositorio.Crear(proyecto);
            return this.RedirectToAction("Index");
        }

        public IActionResult Edit(int id)
        {
            Proyecto proyecto = this.proyectoRepositorio.Obtener(id);
            ViewBag.users = this.GetUsuarios();
            ViewBag.lineasBase = this.GetLineasBase();
            return this.View(proyecto);
        }

        [HttpPost]
        public IActionResult Edit(Proyecto proyecto)
        {
            this.proyectoRepositorio.Actualizar(proyecto);
            return this.RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            Proyecto proyecto = this.proyectoRepositorio.Obtener(id);
            return this.View(proyecto);
        }

        [HttpPost]
        public IActionResult Delete(Proyecto proyecto)
        {
            if(!String.IsNullOrEmpty(Request.Form["eliminar"]))
            {
                bool borrarOk = this.proyectoRepositorio.Delete(proyecto.Id);
                if (!borrarOk)
                {
                    ViewBag.errorMessage("Ocurrió un error al eliminar el proyecto.");
                    return this.View(proyecto);
                }
            }

            return this.RedirectToAction("Index");
        }

        private List<SelectListItem> GetUsuarios()
        {
            List<Usuario> usuarios = this.usuariosRepositorio.ObtenerTodos();
            List<SelectListItem> usuariosAsignables = new List<SelectListItem>();
            foreach (var usuario in usuarios)
            {
                usuariosAsignables.Add(new SelectListItem()
                {
                    Text = usuario.NombreUsuario,
                    Value = usuario.Id.ToString()
                });
            }
            return usuariosAsignables;
        }

        private List<SelectListItem> GetLineasBase()
        {
            List<LineaBase> lineasBase = this.lineaBaseRepositorio.ObtenerLineasBaseActivas();
            List<SelectListItem> lineasBaseActivas = new List<SelectListItem>();
            foreach (var lineaBase in lineasBase)
            {
                lineasBaseActivas.Add(new SelectListItem()
                {
                    Text = lineaBase.Nombre,
                    Value = lineaBase.Codigo.ToString()
                });
            }
            return lineasBaseActivas;
        }
    }
}
