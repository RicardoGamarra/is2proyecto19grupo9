﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IS2GRUPO9.Models;
using IS2GRUPO9.Repositorios;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IS2GRUPO9.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUsuarioRepositorio usuarioRepositorio;

        public LoginController()
        {
            this.usuarioRepositorio = new UsuarioRepositorio();
        }

        [HttpGet]// GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(LoginViewModel loginModel)
        {
            var usuario = this.usuarioRepositorio.ObtenerUsuario(loginModel.Usuario, loginModel.Password);
            if (usuario != null) {
                if (usuario.RolUsuario.Nombre.ToUpper() == "DESARROLLADOR")
                {
                    return this.RedirectToAction("Index", "Desarrollo", new { idUsuario = usuario.Id });
                }
                else if (usuario.RolUsuario.Nombre.ToUpper() == "ADMINISTRADOR")
                {
                    return this.RedirectToAction("Index", "Administrador", new { idUsuario = usuario.Id });
                } else if (usuario.RolUsuario.Nombre.ToUpper() == "GESTOR")
                {
                    return this.RedirectToAction("Index", "Gestor", new { idUsuario = usuario.Id });
                }
            }
            return this.View(loginModel);
        }

        public IActionResult CerrarSesion()
        {
            return this.RedirectToAction("Index");
        }
    }
}
