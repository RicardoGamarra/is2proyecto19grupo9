﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IS2GRUPO9.Models;
using IS2GRUPO9.Repositorios;
using Microsoft.AspNetCore.Mvc;

namespace IS2GRUPO9.Controllers
{
    public class LineaBaseController : Controller
    {
        private readonly ILineaBaseRepositorio lineaBaseRepositorio;

        public LineaBaseController()
        {
            this.lineaBaseRepositorio = new LineaBaseRepositorio();
        }

        public IActionResult Index()
        {
            List<LineaBase> lineasBase = this.lineaBaseRepositorio.ObtenerTodas();
            return View(lineasBase);
        }

        public IActionResult Crear() {
            return View();
        }

        [HttpPost]
        public IActionResult Crear(LineaBase lineaBase)
        {
            if (this.lineaBaseRepositorio.CrearLineaBase(lineaBase))
            {
                if (!String.IsNullOrEmpty(lineaBase.ItemNames))
                {
                    lineaBase.CrearItemsPorLineaBase();
                }
                return this.RedirectToAction("Index");
            }
            else
            {
                ViewBag.errorMessage = "Ocurrió un error al crear la línea base.";
                return this.View(lineaBase);
            }
        }

        public IActionResult Ver(int id)
        {
            LineaBase lineaBase = this.lineaBaseRepositorio.ObtenerLineaBase(id);
            
            return View(lineaBase);
        }

        public IActionResult Edit(int id)
        {
            LineaBase lineaBase = this.lineaBaseRepositorio.ObtenerLineaBase(id);
            if (lineaBase != null)
            {
                lineaBase.ItemNames = String.Join(",", lineaBase.Items.Select(x => x.Nombre).ToArray());
            }
            return View(lineaBase);
        }

        [HttpPost]
        public IActionResult Edit(LineaBase lineaBase)
        {
            if (ModelState.IsValid)
            {
                if (this.lineaBaseRepositorio.ActualizarLineaBase(lineaBase))
                {
                    lineaBase.ActualizarItemsPorLineaBase();
                    return this.RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Ocurrió un error al actualizar la línea base.");
                    return this.View(lineaBase);
                }
            }
            return this.View(lineaBase);
        }

        public IActionResult Eliminar(int idLineaBase)
        {
            LineaBase lineaBase = this.lineaBaseRepositorio.ObtenerLineaBase(idLineaBase);
            return this.View(lineaBase);
        }

        public IActionResult Eliminar(LineaBase lineaBase)
        {
            if (this.lineaBaseRepositorio.EliminarLineaBase(lineaBase.Codigo))
            {
                return this.RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Ocurrió un error al actualizar la línea base.");
                return this.View(lineaBase);
            }
        }
    }
}