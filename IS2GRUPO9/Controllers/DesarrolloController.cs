﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IS2GRUPO9.Models;
using IS2GRUPO9.Repositorios;
using Microsoft.AspNetCore.Mvc;

namespace IS2GRUPO9.Controllers
{
    public class DesarrolloController : Controller
    {
        private readonly IProyectoRepositorio proyectosRepositorio;
        private readonly IRequerimientoRepostorio requerimientoRepositorio;

        public DesarrolloController()
        {
            this.proyectosRepositorio = new ProyectoRepositorio();
            this.requerimientoRepositorio = new RequerimientoRepositorio();
        }

        public IActionResult Index(int idUsuario)
        {
            List<Proyecto> proyectosUsuario = this.proyectosRepositorio.ObtenerProyectosUsuario(idUsuario);
            return View(proyectosUsuario);
        }

        public IActionResult Ver(int id)
        {
            Proyecto proyecto = this.proyectosRepositorio.Obtener(id);
            return this.View(proyecto);
        }

        public IActionResult AgregarRequerimiento(int id)
        {
            RequerimientoProyecto model = new RequerimientoProyecto();
            model.IdProyecto = id;
            return View(model);
        }

        [HttpPost]
        public IActionResult AgregarRequerimiento(RequerimientoProyecto requerimiento)
        {
            this.requerimientoRepositorio.Crear(requerimiento);
            return this.RedirectToAction("Ver", new { id = requerimiento.IdProyecto });
        }

        public IActionResult EditarRequerimiento(int id)
        {
            RequerimientoProyecto model = requerimientoRepositorio.Obtener(id);
            return View(model);
        }

        [HttpPost]
        public IActionResult EditarRequerimiento(RequerimientoProyecto requerimiento)
        {
            this.requerimientoRepositorio.Actualizar(requerimiento);
            return this.RedirectToAction("Ver", new { id = requerimiento.IdProyecto });
        }

        public IActionResult EliminarRequerimiento(int id)
        {
            RequerimientoProyecto model = requerimientoRepositorio.Obtener(id);
            return this.View(model);
        }

        [HttpPost]
        public IActionResult EliminarRequerimiento(RequerimientoProyecto requerimiento)
        {
            this.requerimientoRepositorio.Eliminar(requerimiento);
            return this.RedirectToAction("Ver", new { id = requerimiento.IdProyecto });
        }
    }
}