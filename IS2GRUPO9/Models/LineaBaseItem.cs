﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Models
{
    [Table("LINEA_BASE_ITEMS")]
    public class LineaBaseItem
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("ID_LINEA_BASE")]
        public int IdLineaBase { get; set; }

        [Column("NOMBRE")]
        public String Nombre { get; set; }
    }
}
