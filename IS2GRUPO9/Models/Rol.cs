﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Models
{
    [Table("ROLES")]
    public class Rol
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("DESCRIPCION")]
        public String Nombre { get; set; }

        [Column("NOMBRE")]
        public String Descripcion { get; set; }
    }
}
