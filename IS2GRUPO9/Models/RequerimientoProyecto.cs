﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Models
{
    [Table("REQUERIMIENTO_PROYECTO")]
    public class RequerimientoProyecto
    {
        [Column("ID")]
        public int Id { get; set; }

        [Column("DESCRIPCION")]
        public String Descripcion { get; set; }

        [Column("PRIORIDAD")]
        public String Prioridad { get; set; }

        [Column("ID_PROYECTO")]
        public int IdProyecto { get; set; }
    }
}
