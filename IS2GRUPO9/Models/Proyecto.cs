﻿using IS2GRUPO9.Repositorios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Models
{
    [Table("PROYECTOS")]
    public class Proyecto
    {
        private readonly ILineaBaseRepositorio lineaBaseRepositorio;
        private readonly IProyectoRepositorio proyectoRepositorio;

        private readonly IUsuarioRepositorio usuarioRepositorio;
        public Proyecto()
        {
            this.lineaBaseRepositorio = new LineaBaseRepositorio();
            this.usuarioRepositorio = new UsuarioRepositorio();
            this.proyectoRepositorio = new ProyectoRepositorio();
        }
        
        [Column("ID")]
        public int Id { get; set; }

        [Column("NOMBRE")]
        public String Nombre { get; set; }

        [Column("DESCRIPCION")]
        public String Descripcion { get; set; }

        [Column("FECHA_CREACION")]
        public DateTime FechaCreacion { get; set; }

        [Column("ID_ENCARGADO")]
        public int IdEncargado { get; set; }

        [Column("ID_LINEA_BASE")]
        public int IdLineaBase { get; set; }

        [NotMapped]
        public LineaBase LineaBaseProyecto { get { return this.lineaBaseRepositorio.ObtenerLineaBase(this.IdLineaBase); } }

        [NotMapped]
        public Usuario UsuarioEncargado
        {
            get { return this.usuarioRepositorio.ObtenerUsuario(this.IdEncargado); }
        }

        [NotMapped]
        public List<RequerimientoProyecto> Requerimientos { get { return this.proyectoRepositorio.ObtenerRequerimientos(this.Id); } }

        public void AgregarRequerimiento(String descripcion, String prioridad)
        {
            var requerimiento = new RequerimientoProyecto();
            requerimiento.IdProyecto = this.Id;
            requerimiento.Descripcion = descripcion;
            requerimiento.Prioridad = prioridad;
            this.proyectoRepositorio.AgregarRequerimiento(requerimiento);
        }
    }
}
