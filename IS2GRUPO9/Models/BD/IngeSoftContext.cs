﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IS2GRUPO9.Models;

namespace IS2GRUPO9.Models.BD
{
    public class IngeSoftContext : DbContext
    {
        public DbSet<Proyecto> Proyectos { get; set; }

        public DbSet<Rol> Roles { get; set; }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<LineaBase> LineasBase { get; set; }

        public DbSet<LineaBaseItem> LineaBaseItems { get; set; }

        public DbSet<RequerimientoProyecto> RequerimientosProyectos { get; set; }

        public IngeSoftContext()
        {

        }

        public IngeSoftContext(DbContextOptions<IngeSoftContext> options)
            : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
               .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
               .AddJsonFile("appsettings.json")
               .Build();
            optionBuilder.UseSqlServer(configuration.GetConnectionString("AppDbContext"));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Proyecto>()
                .HasKey(b => b.Id);
            modelBuilder.Entity<Rol>()
                .HasKey(b => b.Id);
            modelBuilder.Entity<Usuario>()
                .HasKey(b => b.Id);
            modelBuilder.Entity<LineaBase>()
                .HasKey(b => b.Codigo);
            modelBuilder.Entity<LineaBaseItem>()
                .HasKey(b => b.Id);
            modelBuilder.Entity<RequerimientoProyecto>()
                .HasKey(b => b.Id);
        }
    }
}
