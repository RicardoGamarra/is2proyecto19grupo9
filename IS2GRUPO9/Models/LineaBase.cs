﻿using IS2GRUPO9.Repositorios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Models
{
    [Table("LINEA_BASE")]
    public class LineaBase
    {
        public static String ESTADO_ACTIVA = "A";

        private readonly ILineaBaseItemRepositorio lineaBaseItemRepositorio;

        public LineaBase()
        {
            this.lineaBaseItemRepositorio = new LineaBaseItemRepositorio();
        }

        [Column("ID")]
        public int Codigo { get; set; }

        [Column("NOMBRE")]
        public string Nombre { get; set; }

        [Column("ESTADO")]
        public String Estado { get; set; }
        
        [NotMapped]
        public String ItemNames { get; set; }

        [NotMapped]
        public List<Fase> Fases { get; set; }

        [NotMapped]
        public List<LineaBaseItem> Items { get {
                return this.lineaBaseItemRepositorio.ObtenerPorLineaBase(this.Codigo);
            }}

        public void CrearItemsPorLineaBase()
        {
            this.lineaBaseItemRepositorio.AgregarItems(this.Codigo, this.ItemNames.Split(','));
        }

        public void ActualizarItemsPorLineaBase()
        {
            _ = this.lineaBaseItemRepositorio.ActualizarItemsPorLineaBase(this.Codigo, this.ItemNames.Split(','));
        }
    }
}
