﻿using IS2GRUPO9.Repositorios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Models
{
    [Table("USUARIOS")]
    public class Usuario
    {
        private readonly IRolRepositorio rolRepositorio;

        public Usuario()
        {
            this.rolRepositorio = new RolRepositorio();
        }

        [Column("ID")]
        public int Id { get; set; }

        [Column("NOMBRE_USUARIO")]
        public string NombreUsuario { get; set; }

        [Column("NOMBRE")]
        public string Nombre { get; set; }

        [Column("APELLIDO")]
        public string Apellido { get; set; }

        [Column("EMAIL")]
        public String Email { get; set; }

        [Column("PASS")]
        public string Password { get; set; }

        [Column("ID_ROL")]
        public int IdRol { get; set; }

        [Column("ROL")]
        public string Rol { get; set; }

        [NotMapped]
        public Rol RolUsuario { get { return this.rolRepositorio.Obtener(this.IdRol); } }
    }
}
