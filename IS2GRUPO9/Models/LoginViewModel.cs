﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Models
{
    public class LoginViewModel
    {
        public String Usuario { get; set; }

        public String Password { get; set; }
    }
}
