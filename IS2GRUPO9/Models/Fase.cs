﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Models
{
    public class Fase
    {
        public Fase()
        {
            this.Items = new List<Item>();
        }
        public String CodigoFase { get; set; }

        public String Nombre { get; set; }

        public List<Item> Items { get; set; }

        public String Descripcion { get; set; }
    }
}
