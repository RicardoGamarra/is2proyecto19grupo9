﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IS2GRUPO9.Models
{
    public class Item
    {
        public int Codigo { get; set; }

        public String Nombre { get; set; }

        public String Descripcion { get; set; }

        public int Prioridad { get; set; }

        public String Version { get; set; }

        public String CodigoFase { get; set; }

        public String[] Observaciones { get; set; }

        public Usuario UsuarioAsignado { get; set; }

        public String CodUsuarioAsignado { get; set; }
    }
}